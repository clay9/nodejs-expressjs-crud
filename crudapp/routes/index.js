var express = require('express');
var router = express.Router();
var connection = require('../config/connection');

/* GET home page. */
router.get('/', function(req, res) {
  connection.query('SELECT * FROM users',function (err,rows) {
      if(err) {
          throw err;
      }
      else{
          // console.log(rows);
          res.render('index', { title: 'Sample' ,users:rows});
      }

  });

});

//adduser route
router.post('/adduser',function (req,res) {
    const userdata={
        fname:req.body.fname,
        lname:req.body.lname,
        email:req.body.email,
        profession:req.body.profession
    }

    connection.query('INSERT INTO users SET ?',userdata,function (err,result) {
        if(err) throw err;
        res.redirect('/');
        
    })
    // res.send("data inserted")
    
});

//delete user route
router.get('/deleteuser/:id',function (req,res) {

    var userid=req.params.id;

    connection.query('DELETE FROM users WHERE id=?',[userid],function (err,result) {
        if(err) throw err;
        res.redirect('/');

    })
});

//get userdata route
router.get('/edit/:id',function (req,res) {

    var userid=req.params.id;
    connection.query("SELECT * FROM users WHERE id=?",[userid],function (err,rows) {
        if(err) throw err;
        res.render('edit',{userdata:rows});
    })


});

//update user route
router.post('/updateUser/:id',function (req,res) {

        var fname=req.body.fname;
        var lname=req.body.lname;
        var email=req.body.email;
        var profession=req.body.profession;

        var updateid=req.params.id;



        connection.query("UPDATE users SET fname=?,lname=?,email=?,profession=? WHERE id=?",[fname,lname,email,profession,updateid],function (err,respond) {
            if(err) throw err;
            res.redirect('../../');
        })




});



module.exports = router;
